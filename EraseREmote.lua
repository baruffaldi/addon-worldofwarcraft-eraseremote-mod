-----------------------------------------------------------------------------------
---    Eraser Guild Emote MOD
---    Coded by mdb
---    mdb@insaneminds.org
---    www.insaneminds.org/wow/mods
---	 Changes: -
---	 
---	 ToDo:-Numero Canale 10 con autojoin
---		 -New Mp3z (ninjaaa)
------------------------------------------------------------------------------------ 


function EraseREmote_OnLoad()

	-- Notify the succesful loading of this mod
	if( DEFAULT_CHAT_FRAME ) then
		DEFAULT_CHAT_FRAME:AddMessage("EraseR Guild Emote Mod has been loaded.");
	end

	UIErrorsFrame:AddMessage("[*] Emote Mod Loaded - EraseR Guild [*]", 1.0, 1.0, 1.0, 1.0, UIERRORS_HOLD_TIME);	

	-- Register the events to bind addicted scripts
    this:RegisterEvent("VARIABLES_LOADED");       
    this:RegisterEvent("CHAT_MSG_CHANNEL"); 
	this:RegisterEvent("ADDON_LOADED");
	this:RegisterEvent("PLAYER_ENTERING_WORLD");
	
end

function EraseREmote_OnEvent()
	
	-- Init script
    if (event == "VARIABLES_LOADED") then
        EraseREmote_init();
    end

	-- Autojoin	
	if (event == "PLAYER_ENTERING_WORLD") then
		--emoteon_command();
	end
	
	-- Capture channel messages
	if (event == "CHAT_MSG_CHANNEL") then
		
		if (arg9 == "EraseREmote") then

			result = string.find( arg1, "^Mettiamo in coda.*");
			if (result ~= nil) then
					if (string.find( arg1, "<MDB>" .. GetZoneText() )) then
							PlaySoundFile("Interface\\AddOns\\EraseREmote\\noises\\queue.mp3");
							UIErrorsFrame:AddMessage(arg2 .. " Mettiamo in coda", 1.0, 1.0, 1.0, 1.0, UIERRORS_HOLD_TIME);
					end
			end

			result = string.find( arg1, "^Tutti al centro!!.*");
			if (result ~= nil) then
					if (string.find( arg1, "<MDB>" .. GetZoneText() )) then
							PlaySoundFile("Interface\\AddOns\\EraseREmote\\noises\\tutticentro.mp3");
							UIErrorsFrame:AddMessage(arg2 .. " Tutti al centro!!", 1.0, 1.0, 1.0, 1.0, UIERRORS_HOLD_TIME);
					end
			end

			result = string.find( arg1, "^Ci sono 1 2 3.*");
			if (result ~= nil) then
					if (string.find( arg1, "<MDB>" .. GetZoneText() )) then
							PlaySoundFile("Interface\\AddOns\\EraseREmote\\noises\\12345critter.mp3");
							UIErrorsFrame:AddMessage(arg2 .. " Ci sono 1.. 2.. 3..", 1.0, 1.0, 1.0, 1.0, UIERRORS_HOLD_TIME);
					end
			end

			result = string.find( arg1, "^Combatti villano!!.*");
			if (result ~= nil) then
					if (string.find( arg1, "<MDB>" .. GetZoneText() )) then
							PlaySoundFile("Interface\\AddOns\\EraseREmote\\noises\\staffataetcetc.mp3");
							UIErrorsFrame:AddMessage(arg2 .. " Combatti villano!!", 1.0, 1.0, 1.0, 1.0, UIERRORS_HOLD_TIME);
					end
			end

			result = string.find( arg1, "^Help faaarm!!.*");
			if (result ~= nil) then
					if (string.find( arg1, "<MDB>" .. GetZoneText() )) then
							PlaySoundFile("Interface\\AddOns\\EraseREmote\\noises\\helpfarm.mp3");
							UIErrorsFrame:AddMessage(arg2 .. " Help faaarm!!", 1.0, 1.0, 1.0, 1.0, UIERRORS_HOLD_TIME);
					end
			end

			result = string.find( arg1, "^Stiamo vincendo.*");
			if (result ~= nil) then
					if (string.find( arg1, "<MDB>" .. GetZoneText() )) then
							PlaySoundFile("Interface\\AddOns\\EraseREmote\\noises\\stiamovincendo.mp3");
							UIErrorsFrame:AddMessage(arg2 .. " Stiamo vincendo!!", 1.0, 1.0, 1.0, 1.0, UIERRORS_HOLD_TIME);
					end
			end


			result = string.find( arg1, "^C'e' qualcuno?.*");
			if (result ~= nil) then
					if (string.find( arg1, "<MDB>" .. GetZoneText() )) then
							PlaySoundFile("Interface\\AddOns\\EraseREmote\\noises\\dasolo.mp3");
							UIErrorsFrame:AddMessage(arg2 .. " C'e' qualcuno?", 1.0, 1.0, 1.0, 1.0, UIERRORS_HOLD_TIME);
					end
			end

			result = string.find( arg1, "^Priest DOT!!.*");
			if (result ~= nil) then
					if (string.find( arg1, "<MDB>" .. GetZoneText() )) then
							PlaySoundFile("Interface\\AddOns\\EraseREmote\\noises\\dot.mp3");
							UIErrorsFrame:AddMessage(arg2 .. " Priest DOT!!", 1.0, 1.0, 1.0, 1.0, UIERRORS_HOLD_TIME);
					end
			end

			result = string.find( arg1, "^DOT, DOT.. Bravi.*");
			if (result ~= nil) then
					if (string.find( arg1, "<MDB>" .. GetZoneText() )) then
							PlaySoundFile("Interface\\AddOns\\EraseREmote\\noises\\dotbravi.mp3");
							UIErrorsFrame:AddMessage(arg2 .. " DOT, DOT.. Bravi..", 1.0, 1.0, 1.0, 1.0, UIERRORS_HOLD_TIME);
					end
			end
			
			result = string.find( arg1, "^Good Night.*");
			if (result ~= nil) then
					if (string.find( arg1, "<MDB>" .. GetZoneText() )) then
							PlaySoundFile("Interface\\AddOns\\EraseREmote\\noises\\tossico.mp3");
							UIErrorsFrame:AddMessage(arg2 .. " Good Night ^_^", 1.0, 1.0, 1.0, 1.0, UIERRORS_HOLD_TIME);
					end
			end
		
			result = string.find( arg1, "^Sings 'Non lo faccio piu', uh!'.*");
			if (result ~= nil) then
					if (string.find( arg1, "<MDB>" .. GetZoneText() )) then
							PlaySoundFile("Interface\\AddOns\\EraseREmote\\noises\\senzacuore.mp3");
							UIErrorsFrame:AddMessage(arg2 .. " Sings 'Non lo faccio piu', uh!'", 1.0, 1.0, 1.0, 1.0, UIERRORS_HOLD_TIME);
					end
			end
			
			result = string.find( arg1, "^Sings 'Poh poh poh uh, uh!'.*");
			if (result ~= nil) then
					if (string.find( arg1, "<MDB>" .. GetZoneText() )) then
							PlaySoundFile("Interface\\AddOns\\EraseREmote\\noises\\hill.mp3");
							UIErrorsFrame:AddMessage(arg2 .. " Sings 'Poh poh poh uh, uh!'", 1.0, 1.0, 1.0, 1.0, UIERRORS_HOLD_TIME);
					end
			end
				
			result = string.find( arg1, "^: Udar has left the guild.*");
			if (result ~= nil) then
					if (string.find( arg1, "<MDB>" .. GetZoneText() )) then
							PlaySoundFile("Interface\\AddOns\\EraseREmote\\noises\\grave.mp3");
							UIErrorsFrame:AddMessage(arg2 .. ": Udar has left the guild ", 1.0, 1.0, 1.0, 1.0, UIERRORS_HOLD_TIME);
					end
			end
			
			result = string.find( arg1, "^calls out for healing.*");
			if (result ~= nil) then
					if (string.find( arg1, "<MDB>" .. GetZoneText() )) then
							PlaySoundFile("Interface\\AddOns\\EraseREmote\\noises\\scildatemi.mp3");
							UIErrorsFrame:AddMessage(arg2 .. " calls out for healing", 1.0, 1.0, 1.0, 1.0, UIERRORS_HOLD_TIME);
					end
			end
			
			result = string.find( arg1, "^flirts.*");
			if (result ~= nil) then
					if (string.find( arg1, "<MDB>" .. GetZoneText() )) then
							PlaySoundFile("Interface\\AddOns\\EraseREmote\\noises\\fiky.mp3");
							UIErrorsFrame:AddMessage(arg2 .. " flirts", 1.0, 1.0, 1.0, 1.0, UIERRORS_HOLD_TIME);
					end
			end
		
			result = string.find( arg1, "^Everybody give a cheer, hurray!.*");
			if (result ~= nil) then
					if (string.find( arg1, "<MDB>" .. GetZoneText() )) then
							PlaySoundFile("Interface\\AddOns\\EraseREmote\\noises\\wonka.mp3");
							UIErrorsFrame:AddMessage(arg2 .. " Everybody give a cheer, hurray!", 1.0, 1.0, 1.0, 1.0, UIERRORS_HOLD_TIME);
					end
			end
			
			result = string.find( arg1, "^'Mbuto!.*");
			if (result ~= nil) then
					if (string.find( arg1, "<MDB>" .. GetZoneText() )) then
							PlaySoundFile("Interface\\AddOns\\EraseREmote\\noises\\mbuto.mp3");
							UIErrorsFrame:AddMessage(arg2 .. " 'Mbuto!", 1.0, 1.0, 1.0, 1.0, UIERRORS_HOLD_TIME);
					end
			end
			
			result = string.find( arg1, "^Falegnameria, Legno, Corteccia, Alberi, Carta, Cartone!.*");
			if (result ~= nil) then
					if (string.find( arg1, "<MDB>" .. GetZoneText() )) then
							PlaySoundFile("Interface\\AddOns\\EraseREmote\\noises\\legno.mp3");
							UIErrorsFrame:AddMessage(arg2 .. " Falegnameria, Legno, Corteccia, Alberi, Carta, Cartone!", 1.0, 1.0, 1.0, 1.0, UIERRORS_HOLD_TIME);
					end
			end
			
			result = string.find( arg1, "^Pikachu!.*");
			if (result ~= nil) then
					if (string.find( arg1, "<MDB>" .. GetZoneText() )) then
							PlaySoundFile("Interface\\AddOns\\EraseREmote\\noises\\pikachu.mp3");
							UIErrorsFrame:AddMessage(arg2 .. " Pikachu!", 1.0, 1.0, 1.0, 1.0, UIERRORS_HOLD_TIME);
					end
			end
			
			result = string.find( arg1, "^ProTTooh?.*");
			if (result ~= nil) then
					if (string.find( arg1, "<MDB>" .. GetZoneText() )) then
						magicn = math.random(6);
							PlaySoundFile("Interface\\AddOns\\EraseREmote\\noises\\protto" .. magicn .. ".mp3");
							UIErrorsFrame:AddMessage(arg2 .. " ProTTooh?", 1.0, 1.0, 1.0, 1.0, UIERRORS_HOLD_TIME);
					end
			end
			
			result = string.find( arg1, "^BASTAAAAAAAAAA!.*");
			if (result ~= nil) then
					if (string.find( arg1, "<MDB>" .. GetZoneText() )) then
							PlaySoundFile("Interface\\AddOns\\EraseREmote\\noises\\basta.mp3");
							UIErrorsFrame:AddMessage(arg2 .. " BASTAAAAAAAAAA!", 1.0, 1.0, 1.0, 1.0, UIERRORS_HOLD_TIME);
					end
			end
			 
			result = string.find( arg1, "^dotate sto cazo di mob.*");
			if (result ~= nil) then
					if (string.find( arg1, "<MDB>" .. GetZoneText() )) then
							PlaySoundFile("Interface\\AddOns\\EraseREmote\\noises\\dotate.mp3");
							UIErrorsFrame:AddMessage(arg2 .. " dotate sto cazo di mob", 1.0, 1.0, 1.0, 1.0, UIERRORS_HOLD_TIME);
					end
			end
			
		end
		
	end
	
end

function EraseREmote_init()

	-- Switch to invisible the channel
	RemoveChatWindowChannel(DEFAULT_CHAT_FRAME:GetID(), "EraseREmote");

	-- Bind slash commands
	SlashCmdList["musica"] = musica_command;
	SLASH_musica1 = "/senzacuore";
	SlashCmdList["hill"] = hill_command;
	SLASH_hill1 = "/bennyhill";
	SlashCmdList["legno"] = legno_command;
	SLASH_legno1 = "/legno";
	SlashCmdList["wonka"] = wonka_command;
	SLASH_wonka1 = "/hurray";
	SlashCmdList["mbuto"] = mbuto_command;
	SLASH_mbuto1 = "/mbuto";
    SlashCmdList["pikachu"] = pikachu_command;
	SLASH_pikachu1 = "/pikachu";
	
    SlashCmdList["protto"] = protto_command;
	SLASH_protto1 = "/protto";
    SlashCmdList["basta"] = basta_command;
	SLASH_basta1 = "/basta";
    SlashCmdList["grave"] = grave_command;
	SLASH_grave1 = "/grave";
    SlashCmdList["dotate"] = dotate_command;
	SLASH_dotate1 = "/dot";
    SlashCmdList["healatemi"] = healatemi_command;
	SLASH_healatemi1 = "/healatemi";
    SlashCmdList["fiky"] = fiky_command;
	SLASH_fiky1 = "/cistai";
    SlashCmdList["dotbravi"] = dotbravi_command;
	SLASH_dotbravi1 = "/bravi";
    SlashCmdList["dot"] = dot_command;
	SLASH_dot1 = "/dot2";
    SlashCmdList["tossico"] = tossico_command;
	SLASH_tossico1 = "/goodnight";
    SlashCmdList["queue"] = queue_command;
	SLASH_queue1 = "/queue";
    SlashCmdList["blacksmith"] = blacksmith_command;
	SLASH_blacksmith1 = "/blacksmith";
    SlashCmdList["critter"] = critter_command;
	SLASH_critter1 = "/critter";
    SlashCmdList["combatti"] = combatti_command;
	SLASH_combatti1 = "/combatti";
    SlashCmdList["farm"] = farm_command;
	SLASH_farm1 = "/farm";
    SlashCmdList["vinciamo"] = vinciamo_command;
	SLASH_vinciamo1 = "/vinciamo";
    SlashCmdList["chice"] = chice_command;
	SLASH_chice1 = "/chice";

    SlashCmdList["emoteoff"] = emoteoff_command;
	SLASH_emoteoff1 = "/emoteoff";
    SlashCmdList["emoteon"] = emoteon_command;
	SLASH_emoteon1 = "/emoteon";
	SlashCmdList["emotelist"] = listhelp_command;
	SLASH_emotelist1 = "/emotelist";
	SlashCmdList["emotewho"] = emotewho_command;
	SLASH_emotewho1 = "/emotewho";
	SlashCmdList["emotereload"] = emotereload_command;
	SLASH_emotereload1 = "/emotereload";
	
end

function queue_command(msg)
    SendChatMessage("Mettiamo in coda" .. "<MDB>" .. GetZoneText(),"CHANNEL",nil,GetChannelName("EraseREmote"));
end

function blacksmith_command(msg)
    SendChatMessage("Tutti al centro!!" .. "<MDB>" .. GetZoneText(),"CHANNEL",nil,GetChannelName("EraseREmote"));
end

function critter_command(msg)
    SendChatMessage("Ci sono 1 2 3" .. "<MDB>" .. GetZoneText(),"CHANNEL",nil,GetChannelName("EraseREmote"));
end

function combatti_command(msg)
    SendChatMessage("Combatti villano!!" .. "<MDB>" .. GetZoneText(),"CHANNEL",nil,GetChannelName("EraseREmote"));
end

function farm_command(msg)
    SendChatMessage("Help faaarm!!" .. "<MDB>" .. GetZoneText(),"CHANNEL",nil,GetChannelName("EraseREmote"));
end

function vinciamo_command(msg)
    SendChatMessage("Stiamo vincendo" .. "<MDB>" .. GetZoneText(),"CHANNEL",nil,GetChannelName("EraseREmote"));
end

function chice_command(msg)
    SendChatMessage("C'e' qualcuno?" .. "<MDB>" .. GetZoneText(),"CHANNEL",nil,GetChannelName("EraseREmote"));
end

function dotbravi_command(msg)
    SendChatMessage("DOT, DOT.. Bravi" .. "<MDB>" .. GetZoneText(),"CHANNEL",nil,GetChannelName("EraseREmote"));
end

function dot_command(msg)
    SendChatMessage("Priest DOT!!" .. "<MDB>" .. GetZoneText(),"CHANNEL",nil,GetChannelName("EraseREmote"));
end

function tossico_command(msg)
    SendChatMessage("Good Night" .. "<MDB>" .. GetZoneText(),"CHANNEL",nil,GetChannelName("EraseREmote"));
end

function musica_command(msg)
    SendChatMessage("Sings 'Non lo faccio piu', uh!'" .. "<MDB>" .. GetZoneText(),"CHANNEL",nil,GetChannelName("EraseREmote"));

end

function hill_command(msg)
    SendChatMessage("Sings 'Poh poh poh uh, uh!'" .. "<MDB>" .. GetZoneText(),"CHANNEL",nil,GetChannelName("EraseREmote"));
end

function legno_command(msg)
    SendChatMessage("Falegnameria, Legno, Corteccia, Alberi, Carta, Cartone!" .. "<MDB>" .. GetZoneText(),"CHANNEL",nil,GetChannelName("EraseREmote"));
end

function wonka_command(msg)
    SendChatMessage("Everybody give a cheer, hurray!" .. "<MDB>" .. GetZoneText(),"CHANNEL",nil,GetChannelName("EraseREmote"));
end

function mbuto_command(msg)
    SendChatMessage("'Mbuto!" .. "<MDB>" .. GetZoneText(),"CHANNEL",nil,GetChannelName("EraseREmote"));
end

function pikachu_command(msg)
    SendChatMessage("Pikachu!" .. "<MDB>" .. GetZoneText(),"CHANNEL",nil,GetChannelName("EraseREmote"));
end

function grave_command(msg)
    SendChatMessage(": Udar has left the guild" .. "<MDB>" .. GetZoneText(),"CHANNEL",nil,GetChannelName("EraseREmote"));
end

function protto_command(msg)
    SendChatMessage("ProTTooh?" .. "<MDB>" .. GetZoneText(),"CHANNEL",nil,GetChannelName("EraseREmote"));
end

function basta_command(msg)
    SendChatMessage("BASTAAAAAAAAAA!" .. "<MDB>" .. GetZoneText(),"CHANNEL",nil,GetChannelName("EraseREmote"));
end

function dotate_command(msg)
    SendChatMessage("dotate sto cazo di mob" .. "<MDB>" .. GetZoneText(),"CHANNEL",nil,GetChannelName("EraseREmote"));
end

function healatemi_command(msg)
    SendChatMessage("calls out for healing" .. "<MDB>" .. GetZoneText(),"CHANNEL",nil,GetChannelName("EraseREmote"));
end

function fiky_command(msg)
	SendChatMessage("flirts" .. "<MDB>" .. GetZoneText(), "CHANNEL", nil, GetChannelName("EraseREmote"));
	DoEmote("flirt");
end

function emoteon_command()
	JoinChannelByName("EraseREmote", nil, DEFAULT_CHAT_FRAME:GetID());
	RemoveChatWindowChannel(DEFAULT_CHAT_FRAME:GetID(), "EraseREmote");
	
	DEFAULT_CHAT_FRAME:AddMessage("EraseR Guild Emote Mod has been activated.");
end

function emoteoff_command()
	LeaveChannelByName("EraseREmote");
	
	DEFAULT_CHAT_FRAME:AddMessage("EraseR Guild Emote Mod has been disabled.");
end

function emotereload_command()
	loaded, reason = LoadAddOn("EraseREmote")
	ConsoleExec("reloadui");
end

function listhelp_command()
	DEFAULT_CHAT_FRAME:AddMessage("EraseR Emote MOD");
	DEFAULT_CHAT_FRAME:AddMessage("-----=-=-=------");
	DEFAULT_CHAT_FRAME:AddMessage("Sounds:");
	DEFAULT_CHAT_FRAME:AddMessage("-Original: senzacuore - bennyhill - legno - hurray - mbuto - pikachu");
	DEFAULT_CHAT_FRAME:AddMessage("-Leet: protto - basta - grave - dot - dot2 - bravi - goodnight - healatemi - cistai - queue - blacksmith - critter - combatti - farm - vinciamo - chice");
	DEFAULT_CHAT_FRAME:AddMessage("");
	DEFAULT_CHAT_FRAME:AddMessage("Misc:");
--	DEFAULT_CHAT_FRAME:AddMessage("emotewho - Utenti con il mod attivato");
	DEFAULT_CHAT_FRAME:AddMessage("emoteon - Attiva il mod");
	DEFAULT_CHAT_FRAME:AddMessage("emoteoff - Disattiva il mod");
	DEFAULT_CHAT_FRAME:AddMessage("emotereload - Reloada l'Interfaccia Utente");
end

function emotewho_command()
end
